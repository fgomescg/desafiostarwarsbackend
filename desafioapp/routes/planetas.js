var express = require('express');
var router = express.Router();

const Planeta = require('../models/planeta');
const Axios = require('axios');

const swapi = "https://swapi.co/api/planets/";

/* GET Planetas */
router.get('/', async (req, res) => {
  try {

    const planetas = await Planeta.find();
    
    planetas.map(async planeta => {    

      const response = await Axios({
        method: 'GET',
        url: swapi + "?search=" + planeta.nome      
      })

    planeta.qtdAparicoes = 0;

    if(response.data.count > 0)
      planeta.qtdAparicoes = response.data.results[0].films.length;
   
  });
    
    return res.send({ planetas });

  } catch (err) {
    return res.status(400).send({ error: "Erro ao listar os planetas." });
  }
});

/* GET By Nome */
router.get('/nome/:nome', async (req, res) => {
  try {
    const planeta =  await Planeta.findOne({ nome: req.params.nome });

    const response = await Axios({
      method: 'GET',
      url: swapi + "?search=" + req.params.nome    
    });

    planeta.qtdAparicoes = 0;

    if(response.data.count > 0)
    planeta.qtdAparicoes = response.data.results[0].films.length;
   
   return res.send({ planeta });

  } catch (err) {
    return res.status(400).send({ error: "Erro ao buscar o Planeta." });
  }
});  

/* GET By ID */
router.get('/:planetaID', async (req, res) => {
  try {
    const planeta = await Planeta.findById(req.params.planetaID);

    return res.send({ planeta });
  } catch (err) {
    return res.status(400).send({ error: "Erro ao listar os planetas." });
  }
}); 

/* POST Planeta*/
router.post('/', async (req, res) => {
  try {
    //verifica se já existe o planeta
    if (await Planeta.findOne({ nome: req.body.nome }))
      return res.status(400).send({ error: 'Planeta já cadastrado.' })

    const planeta = await Planeta.create(req.body);

    return res.send({ planeta });

  } catch (err) {
    return res.status(400).send({ error: "Erro ao criar novo planeta." });
  }
});

/* DELETE Planetas */
router.delete('/:planetaNome', async (req, res) => {
  try {
    await Planeta.findOneAndRemove( { nome : req.params.planetaNome });

    return res.send({ status: "Removido com sucesso." });
  } catch (err) {
    return res.status(400).send({ error: "Erro ao deletar o planeta." });
  }
});

module.exports = router;