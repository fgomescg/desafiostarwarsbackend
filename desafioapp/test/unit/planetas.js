describe('Routes Planetas:', () => {

    describe('Route GET /planetas', () => {
        it('deve retornar uma lista de planetas', done => {
            request.get('/planetas')
            .end((err, res) =>{
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body).to.be.an('object');
                expect(res.body.planetas).to.be.an('array');
                done(err);
            });
        }); 
    });
       
    describe('Route POST /planeta', () => {        
        it('deve adicionar um novo planeta', done => {
            request
            .post('/planetas')
            .send({
                nome:"PlanetaTeste",
                clima:"temperate tropical",
                terreno:"jungle rainforests"
            })
            .end((err, res) =>{
                expect(res).to.have.status(200);
                expect(res).to.be.json; 
                expect(res.body.planeta.nome).to.be.eql('PlanetaTeste');
                done(err);
            });
        });
    });
    
    describe('Route GET /planeta by nome', () => {
        it('deve retornar o planeta PlanetaTeste', done => {
            request.get('/planetas/nome/PlanetaTeste')
            .end((err, res) =>{              
                expect(res).to.have.status(200);
                expect(res).to.be.json;          
                expect(res.body).to.be.an('object');                         
                expect(res.body.planeta.nome).to.be.eql("PlanetaTeste");
                done(err);
            });
        });
    });

    describe('Route GET /planeta by ID', () => {
        it('deve retornar o planeta PlanetaTeste', done => {
            request.get('/planetas/nome/PlanetaTeste')
            .end((err, res) =>{               
                request.get('/planetas/'+res.body.planeta._id)
                .end((err, res2) =>{
                    expect(res2.body.planeta.nome).to.be.eql("PlanetaTeste");
                });
                done(err);
            });           
        });
    });   

    describe('Route DELETE /planeta', () => {       
        it('deve deletar o planeta Teste', done => {
            request
            .delete('/planetas/PlanetaTeste')                
            .end((err, res) =>{
                expect(res).to.be.json;   
                expect(res).to.have.status(200);                               
                done(err);
            });
        });
    });  
        
});