import supertest from 'supertest';
import chai from 'chai';
import app from '../../app';

chai.use(require('chai-http'));

global.app = app;
global.request = supertest(app);
global.expect = chai.expect;
