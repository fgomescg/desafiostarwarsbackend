
const mongoose = require('../database');

const PlanetaSchemma = new mongoose.Schema({

    nome: {
        type: String,
        require: true,
        unique: true,
    },
    clima: {
        type: String,
        required: true,
    },
    terreno: {
        type: String,
        require: true,
    },    
    qtdAparicoes: {
        type: Number
    },
    createdAt: {
        type: Date,
        default: Date.now,
    }
});

PlanetaSchemma.pre('save', async function (next) {

    next();
})

const Planeta = mongoose.model('Planeta', PlanetaSchemma);

module.exports = Planeta;